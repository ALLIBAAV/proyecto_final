//window.onload=init(); asigna el valor real de lo que sea la declaración de retorno de las funciones a la propiedad onload
//window.onload=init; mientras que el segundo asigna la función real y se ejecutará después de que la ventana se haya cargado.

//var permite hacer modificaciones en los valores almacenados 
// let tambien permite hacer modificaciones en los valores almacenados, pero solo estara presente en el bloque en donde fue declarada


var jugadas = 0;
var miSumaAnt = 0;

function iniciar_maquina() {

    //Crear un objeto que referencia al elemento HTML
    //let miPantalla = document.getElementById("pantalla");
    let miDivDados = document.getElementById("Dados");


    //Crear tres elementos HTML. Cuadros donde se muestra el resultado de cada dado.

    for (let i = 0; i < 2; i++) {
        let miDivision = document.createElement("div");
        miDivision.setAttribute("id", "figura" + (i + 1))
        miDivision.setAttribute("class", "cuadro")
        //Agregar el elemento creado al elemento que yo desee
        //miPantalla.appendChild(miDivision);
        miDivDados.appendChild(miDivision);
    }

    //let objeto = document.getElementById("figura1");
    //let miImagen1 = "./images/6.png";
    //objeto.style.backgroundImage = "url(" + miImagen1 + ")";
    //objeto.style.backgroundImage = "url(./images/6.png)";
    //objeto.style.backgroundSize = "contain";
    /*
        let otraDivision = document.createElement("div");
        otraDivision.setAttribute("class", "divBoton");
        miPantalla.appendChild(otraDivision);
    
    
        //Crear una división poara cer el premio
        let premioDivision = document.createElement("div");
        premioDivision.setAttribute("id", "divPremio");
        premioDivision.setAttribute("class", "divPremio");
        miPantalla.appendChild(premioDivision);
    
        let miBoton = document.createElement("input");
        miBoton.setAttribute("type", "button");
        miBoton.setAttribute("class", "boton");
        miBoton.setAttribute("value", "Jugar");
        miBoton.setAttribute("onclick", "lanzar_jugada()");
        otraDivision.appendChild(miBoton);
    */
}

function lanzar_jugada() {

    var formulario = "";
    var apu = document.form1.apuesta.value;
    var tip = document.form1.tipo.value;
    console.log(apu);
    console.log(tip);
    var acum = 0;

    let miJugada = realizar_jugada();

    //Crear un objeto que referencie al primer "contenedor"

    let miCuadro = document.getElementById("figura1");
    let miImagen = buscar_imagen(miJugada[0]);
    //let miUrl = "url(" + miImagen + ");";
    miCuadro.style.backgroundImage = "url(" + miImagen + ")";
    miCuadro.style.backgroundSize = "contain";
    //miCuadro.style.backgroundColor = "red";
    miCuadro.innerHTML = miJugada[0];

    miCuadro = document.getElementById("figura2");
    miImagen = buscar_imagen(miJugada[1]);
    miCuadro.style.backgroundImage = "url(" + miImagen + ")";
    miCuadro.style.backgroundSize = "contain";
    miCuadro.innerHTML = miJugada[1];

    jugadas++;
    console.log(jugadas);
    console.log(miJugada);

    let miSuma = miJugada[0] + miJugada[1];
    console.log(miSuma);

    let miMensaje = validar_resultado(miSuma, tip);
    console.log(miMensaje);

    if (miMensaje == "¡HAS GANADO!") {
        acum += apu;
    } else if (miMensaje == "¡HAS PERDIDO!") {
        acum -= apu;
    } else {
        miSumaAnt = miSuma;
    }

    let miRespuesta = [0, 0, 0];
    miRespuesta[0] = miSuma;
    miRespuesta[1] = miMensaje;
    miRespuesta[2] = acum;
    console.log(miRespuesta);

    let miDivRespuesta = document.getElementById("divRespuesta");
    miDivRespuesta.innerHTML = miRespuesta;

    //let miPremio=calcular_premio(miJugada);
    //let miDivPremio=document.getElementById("divPremio");
    //miDivPremio.innerHTML=miPremio;
}

function realizar_jugada() {

    let t = [1, 2, 3, 4, 5, 6];
    let r = [0, 0];

    for (total_selecciones = 0; total_selecciones < 2; total_selecciones++) {

        let indice = Math.round(Math.random() * (t.length - 1));
        r[total_selecciones] = t[indice];
        console.log(t[indice]);
        //r[total_selecciones] = miImagen;
    }
    console.log(r);
    return r;
}

function buscar_imagen(ind) {
    let lados = [
        [1, "./images/1.png"],
        [2, "./images/2.png"],
        [3, "./images/3.png"],
        [4, "./images/4.png"],
        [5, "./images/5.png"],
        [6, "./images/6.png"]
    ];
    let fin = 0;
    let imagen = "";

    for (let i = 0; i < lados.length; i++) {
        console.log(lados[i][0]);
        if (ind == lados[i][0]) {

            imagen = lados[i][1];
            console.log(imagen);
            console.log("Esta es la imagen");
            i = 6;
        }
    }

    return imagen;

}

function validar_resultado(suma_tirada, tipo_pase) {
    let mensaje = "";
    if (suma_tirada == 7 || suma_tirada == 11) {
        if (tipo_pase == 1) {
            // imprimir !has ganado¡
            //imprimir_en("mensaje","¡HAS GANADO!");
            mensaje = "¡HAS GANADO!";
        } else {
            // imprimir !has perdido¡    
            //imprimir_en("mensaje","¡HAS PERDIDO!");
            mensaje = "¡HAS PERDIDO!";
        }
    } else if (suma_tirada == 2 || suma_tirada == 3 || suma_tirada == 12) {
        if (tipo_pase == 1) {
            // imprimir !has perdido¡
            //imprimir_en("mensaje","¡HAS PERDIDO!");
            mensaje = "¡HAS PERDIDO!";
        } else {
            // imprimir !has ganado¡    
            //imprimir_en("mensaje","¡HAS GANADO!");
            mensaje = "¡HAS GANADO";
        }
    } else {
        //imprimir "gana un punto"
        mensaje = "GANA UN PUNTO";
        //volver a funcion lanzar dados 1
        //LA SUMA DE LA TIRADA TIENE QUE DAR EL MISMO NUMERO CON EL QUE GANO EL PUNTO
        //imprimir_en("mensaje","GANA UN PUNTO")
    }

    return mensaje;
}




